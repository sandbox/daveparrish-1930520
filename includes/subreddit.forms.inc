<?php

/**
 * @file
 * Provides Subreddit forms
 */


/**
 * Form for the submit a link to Reddit action.
 */
function subreddit_submit_link_form(&$form_state) {
  $title_default = array_key_exists('title', $_GET) ? urldecode($_GET['title']) : "";

  $subreddit_name = variable_get('subreddit_name', 'all');
  $form['vote_link'] = array(
    '#value' => l(t("Vote on /r/@subreddit", array('@subreddit' => $subreddit_name)),
      'subreddit/subreddit',
      array(
      'attributes' => array(
        'class' => 'vote-link',
        )
      )
  ));
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#size' => 60,
    '#maxlength' => 300,
    '#default_value' => $title_default,
  );
  $url_default = array_key_exists('url', $_GET) ? urldecode($_GET['url']) : "";
  $form['url'] = array(
    '#type' => 'textfield',
    '#title' => t('URL'),
    '#size' => 60,
    '#maxlength' => 300,
    '#default_value' => $url_default,
  );
  $form['reddiquette_message'] = array(
    '#value' => '<p>' . t('Please be mindful of reddit\'s <a target="_blank" href="https://www.reddit.com/rules">few rules</a> and practice <a target="_blank" href="https://www.reddit.com/wiki/reddiquette">good reddiquette</a>.') . '</p>',
  );


  // Fill out the captcha if needed.
  $captcha_id = array_key_exists('captcha', $_GET) ? urldecode($_GET['captcha']) : NULL;
  if ($captcha_id) {
    $form['captcha_image'] = array(
      '#type' => 'markup',
      '#value' => '<img src="https://reddit.com/captcha/' . $captcha_id . '"></img>',
    );
    $form['captcha'] = array(
      '#type' => 'textfield',
      '#size' => 60,
      '#maxlength' => 300,
      '#description' => t('Please fill the CAPTCHA.'),
    );
  }

  $form['submit'] = array('#type' => 'submit', '#value' => t('Submit'));
  return $form;
}

/**
 * Submit form submit action.
 */
function subreddit_submit_link_form_submit($form, &$form_state) {
  module_load_include('inc', 'subreddit', 'includes/subreddit.actions');
  $captcha_answer = array_key_exists('captcha', $form_state['values']) ?
    $form_state['values']['captcha'] : NULL;
  $captcha_id = array_key_exists('captcha', $_GET) ? $_GET['captcha'] : NULL;
  $submit_response = subreddit_submit($form_state['values']['title'],
    $form_state['values']['url'],
    array('captcha' => $captcha_answer, 'iden' => $captcha_id));

  // If the response needs a captcha, then display the same page again with the
  // captcha.
  if ($submit_response != NULL &&
    array_key_exists('json', $submit_response['result']) &&
    array_key_exists('captcha', $submit_response['result']['json'])) {
    drupal_goto('subreddit/submit',
      array(
        'captcha' => urlencode($submit_response['result']['json']['captcha']),
        'title' => urlencode($form_state['values']['title']),
        'url' => urlencode($form_state['values']['url']),
      ));
  }
}

/**
 * Form for the submit a self text to Reddit action.
 */
function subreddit_submit_text_form(&$form_state) {
  // Add CSS for this form.
  drupal_add_css(drupal_get_path('module', 'subreddit') . '/css/submit_text.css');

  $title_default = array_key_exists('title', $_GET) ? urldecode($_GET['title']) : "";

  $subreddit_name = variable_get('subreddit_name', 'all');
  $form['vote_link'] = array(
    '#value' => l(t("Vote on /r/@subreddit", array('@subreddit' => $subreddit_name)),
      'subreddit/subreddit',
      array(
      'attributes' => array(
        'class' => 'vote-link',
        )
      )
  ));
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#size' => 60,
    '#maxlength' => 300,
    '#default_value' => $title_default,
  );
  $text_default = array_key_exists('text', $_GET) ? urldecode($_GET['text']) : "";
  $form['text'] = array(
    '#type' => 'textarea',
    '#title' => t('Text'),
    '#description' => '<span id="submit_text_reddiquette_link">' . t('<a target="_blank" href="https://www.reddit.com/wiki/reddiquette">reddiquette</a>') . '</span><span id="submit_text_formatting_help_link">' . t('<a target="_blank" href="https://www.reddit.com/wiki/commenting#wiki_posting">formatting help</a>') . '</span>',
    '#default_value' => $text_default,
  );
  $form['reddiquette_message'] = array(
    '#value' => '<p>' . t('Please be mindful of reddit\'s <a target="_blank" href="https://www.reddit.com/rules">few rules</a> and practice <a target="_blank" href="https://www.reddit.com/wiki/reddiquette">good reddiquette</a>.') . '</p>',
  );

  // Fill out the captcha if needed.
  $captcha_id = array_key_exists('captcha', $_GET) ? urldecode($_GET['captcha']) : NULL;
  if ($captcha_id) {
    $form['captcha_image'] = array(
      '#type' => 'markup',
      '#value' => '<img src="https://reddit.com/captcha/' . $captcha_id . '"></img>',
    );
    $form['captcha'] = array(
      '#type' => 'textfield',
      '#size' => 60,
      '#maxlength' => 300,
      '#description' => t('Please fill the CAPTCHA.'),
    );
  }

  $form['submit'] = array('#type' => 'submit', '#value' => t('Submit'));
  return $form;
}

/**
 * Submit form submit action.
 */
function subreddit_submit_text_form_submit($form, &$form_state) {
  module_load_include('inc', 'subreddit', 'includes/subreddit.actions');
  $captcha_answer = array_key_exists('captcha', $form_state['values']) ?
    $form_state['values']['captcha'] : NULL;
  $captcha_id = array_key_exists('captcha', $_GET) ? $_GET['captcha'] : NULL;
  $submit_response = subreddit_submit($form_state['values']['title'],
    NULL, array('captcha' => $captcha_answer, 'iden' => $captcha_id),
    NULL, 'self', $form_state['values']['text']);

  // If the response needs a captcha, then display the same page again with the
  // captcha.
  if ($submit_response != NULL &&
    array_key_exists('json', $submit_response['result']) &&
    array_key_exists('captcha', $submit_response['result']['json'])) {
    drupal_goto('subreddit/submit/text',
      array(
        'captcha' => urlencode($submit_response['result']['json']['captcha']),
        'title' => urlencode($form_state['values']['title']),
        'text' => urlencode($form_state['values']['text']),
      ));
  }
}

/**
 * Submit a link to Reddit.
 */
function subreddit_submit_callback($submit_type) {
  if ($submit_type == 'link') {
    drupal_set_message(t('You are submitting a link. The key to a successful submission is interesting content and a descriptive title.'), 'warning', FALSE);
    return drupal_get_form('subreddit_submit_link_form');
  }

  if ($submit_type == 'text') {
    drupal_set_message(t('You are submitting a text-based post. Speak your mind. A title is required, but expanding further in the text field is not. Beginning your title with "vote up if" is a violation of intergalactic law.'), 'warning', FALSE);
    return drupal_get_form('subreddit_submit_text_form');
  }
}

/**
 * Form for configuring the subreddit module.
 */
function subreddit_configure_form(&$form_state) {
  $form['subreddit'] = array(
    '#type' => 'textfield',
    '#title' => t('Subreddit'),
    '#size' => 60,
    '#maxlength' => 300,
    '#description' => t('Enter the subreddit title.'),
    '#default_value' => variable_get('subreddit_name', 'all'),
  );
  //$form['url'] = array(
  //  '#type' => 'textfield',
  //  '#title' => t('URL'),
  //  '#size' => 60,
  //  '#maxlength' => 300,
  //  '#description' => t('Enter a URL for the Reddit link.'),
  //);

  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));
  return $form;
}

/**
 * Submit form submit action.
 */
function subreddit_configure_form_submit($form, &$form_state) {
  variable_set('subreddit_name', $form_state['values']['subreddit']);
  // TODO: Maybe set a message letting the user know that they need to clear the
  // cache for titles to change on the Subreddit page.
  drupal_set_message('Successfuly change Subreddit name to "' .
    $form_state['values']['subreddit'] . "'");
}

/**
 * Configure the Subreddit module callback.
 */
function subreddit_configure_callback() {
  return drupal_get_form('subreddit_configure_form');
}
