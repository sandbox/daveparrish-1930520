<?php

/**
 * @file
 * Provides functions for subreddit specific actions.
 */

/**
 * Vote a story up.
 *
 * Only works if the Reddit OAuth module is enabled and the user has logged in
 * via Reddit.
 *
 * @param string $id
 *   The comment or submission id.
 * @param int $direction
 *   The direction to vote as described by the reddit api.  Valid values
 *   (1,0,-1).
 */
function subreddit_vote($id, $direction=1, $modhash=NULL) {
  if (!module_exists('reddit_oauth')) {
    watchdog('subreddit', 'Tried to vote on a story but the reddit_oauth is not enabled.');
    return t('reddit_oauth is required to be enabled for voting on a story');
  }

  module_load_include('inc', 'reddit_oauth', 'includes/reddit_oauth.actions');
  $response = reddit_oauth_reddit_action("/api/vote", array(
    'dir' => $direction, 'id' => $id, 'uh' => $modhash), 'POST');
  // If the response was a success.  Else we need an error case.

  // With both reddit information and Drupal information,
  // Save this token to the users account.

  if ($response['code'] != 200) {
    drupal_json(array(
      'message' => t("Something went wrong with voting! id $id direction $direction"),
      'id' => $id,
      'direction' => $direction,
      'response' => $response));
  }
  else {
    drupal_json(array(
      'message' => t("Congratulations on the voting! id $id direction $direction"),
      'id' => $id,
      'direction' => $direction,
      'response' => $response));
  }
}

/**
 * Submit a link to Reddit.
 *
 * @param string $title
 *   Less than 300 character title.
 * @param string $url
 *   URL to the story.
 * @param array $captcha
 *   An array in the following format.
 *   'answer' => the_captcha_answer
 *   'iden' => the_captcha_id
 * @param string $sr
 *   The subreddit name.
 * @param string $kind
 *   Specifies the type of submission. Can be 'link' or 'self'.
 * @param string $text
 *   Raw markdown
 *
 * @return array
 *   Return the array recieved from Reddit.
 */
function subreddit_submit($title, $url=NULL, $captcha=NULL, $sr=NULL, $kind='link', $text=NULL) {
  $response = NULL;

  module_load_include('inc', 'reddit_oauth', 'includes/reddit_oauth.helper');
  if (reddit_oauth_is_connected()) {
    $subreddit_name = $sr ? $sr : variable_get('subreddit_name', 'test');
    module_load_include('inc', 'reddit_oauth', 'includes/reddit_oauth.actions');
    $response = reddit_oauth_reddit_action("/api/submit", array(
      'title' => $title,
      'url' => $url,
      'sr' => $subreddit_name,
      'kind' => $kind,
      'api_type' => 'json',
      'captcha' => isset($captcha['captcha']) ? $captcha['captcha'] : NULL,
      'iden' => isset($captcha['iden']) ? $captcha['iden'] : NULL,
      'text' => $text,
      ),
    'POST');

    // Make sure the response was successful.
    if ($response['code'] == 200) {
      // Check the result for a reddit error.
      if (array_key_exists('errors', $response['result']['json']) &&
        count($response['result']['json']['errors']) > 0) {
        drupal_set_message($response['result']['json']['errors'][0][1], 'warning');
      }
      else {
        drupal_set_message(t('Thank you for submitting a new link to Reddit.com'));
      }
    }
    else {
      // Something went wrong so log it.
      watchdog('subreddit', 'Response from subreddit request was a ' . $response['code'] . ' code.');
      drupal_set_message(t('Oops, something went wrong. Please try again.'));
    }
  }
  else {
    watchdog('subreddit', 'User tried to submit a link without being logged in.');
    drupal_set_message(t('Sorry, you need to be logged into Reddit to submit a link.'));
  }

  return $response;
}

/**
 * Determines which method to use in fetching the subreddit.
 *
 * The primary factor for making the decisions is if the user is logged into
 * OAuth.
 *
 * @param string $subreddit
 *   subreddit name.
 *
 * @return json
 *   json for subreddit.
 */
function subreddit_fetch_subreddit_cache($subreddit, $tab) {
  // If the user is logged into OAuth the run the action.
  if (module_exists('reddit_oauth')) {
    module_load_include('inc', 'reddit_oauth', 'includes/reddit_oauth.helper');
    if (reddit_oauth_is_connected()) {
      return subreddit_fetch_subreddit_oauth($subreddit, $tab);
    }
  }

  // If the user is not logged into OAuth then run the normal subreddit fetch.
  return subreddit_fetch_subreddit_cache_non_oauth($subreddit, $tab);
}

/**
 * Fetch the subreddit data from cache.
 *
 * Try to fetch the subreddit data from cache first.
 * If cache is expired then subreddit data is retrieved from reddit.com.
 *
 * @param string $subreddit
 *   subreddit name.
 *
 * @return json
 *   json for subreddit.
 */
//function subreddit_fetch_subreddit_cache_oauth($subreddit) {
//  // Get the cache subreddit data.
//  $cid = "subreddit:user:$subreddit";
//  $cached = cache_get($cid);
//
//}

/**
 * Fetch the subreddit data from reddit.com.
 *
 * @param string $subreddit
 *   subreddit name.
 *
 * @return json
 *   json for subreddit.
 *   OR returns FALSE if the data is invalid.
 */
function subreddit_fetch_subreddit_oauth($subreddit, $tab) {
  module_load_include('inc', 'reddit_oauth', 'includes/reddit_oauth.actions');
  $response = reddit_oauth_reddit_action("/r/$subreddit/$tab.json", array());

  // Make sure the response was successful.
  if ($response['code'] == 200) {
    return $response['result'];
  }

  // Something went wrong so log it.
  watchdog('subreddit', 'Response from subreddit request was a ' . $response['code'] . ' code.');
  return t('Oops, something went wrong.');
}


/**
 * Fetch the subreddit data from cache.
 *
 * Try to fetch the subreddit data from cache first.
 * If cache is expired then subreddit data is retrieved from reddit.com.
 *
 * @param string $subreddit
 *   subreddit name.
 *
 * @return json
 *   json for subreddit.
 */
function subreddit_fetch_subreddit_cache_non_oauth($subreddit, $tab) {
  // Get the cache subreddit data.
  $cid = "subreddit:anonymous:$subreddit:$tab";
  $cached = cache_get($cid);

  // If the cache is expired then get the data from reddit and then cache it.
  if ($cached && $cached->expire > time()) {
    return $cached->data;
  }
  else {
    $data = subreddit_fetch_subreddit_non_oauth($subreddit, $tab);

    if (FALSE == $data) {
      // Sorry, the data is no good. Fallback to the cache and log issue.
      watchdog('subreddit', 'Subreddit data is invalid. Falling back to last cache data.',
        NULL, WATCHDOG_ERROR);
      if ($cached) {
        return $cached->data;
      }
    }

    // Set a 5 minute cache for the subreddit data.
    // TODO: Make the limit customizable in Drupal configuration settings.
    $expire_time = time() + 300;
    cache_set($cid, $data, 'cache', $expire_time);
    return $data;
  }
}

/**
 * Fetch the subreddit data from reddit.com.
 *
 * @param string $subreddit
 *   subreddit name.
 *
 * @return json
 *   json for subreddit.
 *   OR returns FALSE if the data is invalid.
 */
function subreddit_fetch_subreddit_non_oauth($subreddit, $tab) {
  $return = "";

  // Create a new CURL resource.
  $ch = curl_init();

  // Set URL and other appropriate options.
  $curl_url = "https://www.reddit.com/r/$subreddit/$tab.json";
  $options = array(
    CURLOPT_URL => $curl_url,
    CURLOPT_HEADER => 0,
    CURLOPT_RETURNTRANSFER => TRUE,
    CURLOPT_TIMEOUT => 10,
    CURLOPT_USERAGENT => 'Subreddit Drupal module (dev version) by /u/dmp1ce',
    // Bypass SSL verification. ( Makes application vulerable to Man in the
    // middle attacks! )
    //CURLOPT_SSL_VERIFYPEER => FALSE,
    //CURLOPT_SSL_VERIFYHOST => 0,
  );
  curl_setopt_array($ch, $options);

  // Grab URL and pass it to the browser.
  $result = curl_exec($ch);
  if (!$result) {
    watchdog('subreddit', 'An error with cURL. !error',
      array('!error' => curl_error($ch)), WATCHDOG_ERROR);
    $return = FALSE;
  }
  // Let administrators know when the system is GETting data from Reddit.
  watchdog('subreddit', 'GET URL !url',
    array('!url' => $curl_url), WATCHDOG_INFO);

  // Convert json to PHP array.  We never want to see just the json.
  // TODO: If decode fails then we need to return an error.
  $json_decoded = json_decode($result, TRUE);

  $return = $json_decoded;
  if (!subreddit_validate_data($return)) {
    watchdog('subreddit', 'Subreddit data is not valid. !data',
      array('!data' => $result), WATCHDOG_ERROR);
    $return = FALSE;
  }

  // Close CURL resource, and free up system resources.
  curl_close($ch);

  return $return;
}

/**
 * Determines if the data is "good".
 *
 * This function answers the question, "Should we save this data or try again?"
 *
 * @return bool
 *   TRUE if the data is valid or FALSE if the data is invalid.
 */
function subreddit_validate_data($data) {
  if (is_array($data) && array_key_exists('data', $data)) {
    return TRUE;
  }
  return FALSE;
}
