<?php

/**
 * @file
 * Provides Subreddit pages
 */

/**
 * Create the Subreddit page.
 */
function subreddit_page_callback($tab) {
  $subreddit_name = variable_get('subreddit_name', 'all');

  module_load_include('inc', 'subreddit', 'includes/subreddit.actions');
  $data = subreddit_fetch_subreddit_cache($subreddit_name, $tab);

  if ($data) {
    // If the user is not logged into Drupal then warn them that they
    // must be logged in to use the voting features.
    $current_page_path = substr($_SERVER['REQUEST_URI'], 1);
    $current_page_path = $current_page_path ? $current_page_path : '<front>';
    if (!user_is_logged_in()) {
      drupal_set_message(t('In order to vote and submit you need to be logged in and connected with Reddit. <a href="/user?destination=@current_page">Log in here</a>.',
      array('@current_page' => $current_page_path)), 'warning', FALSE);
    }
    elseif (!reddit_oauth_is_connected()) {
      drupal_set_message(t('In order to vote and submit you need to <a href="/reddit_oauth/connect?return_url=@current_page">connect with Reddit</a>.',
      array('@current_page' => $current_page_path)), 'warning', FALSE);
    }
    $content = subreddit_vote_block_render($subreddit_name, $data);
  }
  else {
    $content = t('Empty dataset. Something went wrong.');
  }

  return $content;
}
