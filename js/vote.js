Drupal.behaviors.subreddit = function(context) {
  $('div.vote-action:not(.subreddit-processed)',
    context).addClass('subreddit-processed')
    .bind('click', function(){

    // Grab the href from the link and use get that data to vote.
    var vote_status = $(this).parents('.inner-thing').attr('class')
    var thing_id = $(this).parents().eq(2).attr('data-fullname')
    var vote_direction = $(this).attr('vote-direction')
    var vote_direction_value = determine_vote_direction(vote_status, vote_direction)
    $.get('/subreddit/vote/' + thing_id  + '/' +
      vote_direction_value, null,
      function(data, textStatus) {
        if (textStatus == 'success') {
          //console.log("Vote Success")
        } else {
          //console.log("Vote fail")
          //console.log(data)
        }
      })

    // After we have sent the vote signal on it's way. Change the voting
    // display to signal to the user that the vote has been made (most likely).
    apply_vote(vote_direction_value, thing_id)

    return false
  })

  /**
   * Changes the HTML based on the vote direction and thing_id
   */
  function apply_vote(vote_direction, thing_id) {
    // If the response was a success then adjust the score and update vote links
    var vote_item = $("div[data-fullname='" + thing_id + "'] div div div.vote-action")
    if(vote_direction == 1) {
      vote_item.filter("[vote-direction='up']").parents('.inner-thing').addClass('likes')
      vote_item.filter("[vote-direction='up']").parents('.inner-thing').removeClass('unvoted')
      vote_item.filter("[vote-direction='up']").parents('.inner-thing').removeClass('dislikes')
      vote_item.filter("[vote-direction='up']").addClass('upmod')
      vote_item.filter("[vote-direction='up']").removeClass('up')
      vote_item.filter("[vote-direction='down']").addClass('down')
      vote_item.filter("[vote-direction='down']").removeClass('downmod')
    }
    else if(vote_direction == -1) {
      vote_item.filter("[vote-direction='down']").parents('.inner-thing').addClass('dislikes')
      vote_item.filter("[vote-direction='down']").parents('.inner-thing').removeClass('unvoted')
      vote_item.filter("[vote-direction='down']").parents('.inner-thing').removeClass('likes')
      vote_item.filter("[vote-direction='down']").addClass('downmod')
      vote_item.filter("[vote-direction='down']").removeClass('down')
      vote_item.filter("[vote-direction='up']").addClass('up')
      vote_item.filter("[vote-direction='up']").removeClass('upmod')
    }
    else if(vote_direction == 0) {
      vote_item.parents('.inner-thing').addClass('unvoted')
      vote_item.parents('.inner-thing').removeClass('dislikes')
      vote_item.parents('.inner-thing').removeClass('likes')
      vote_item.removeClass('downmod')
      vote_item.removeClass('upmod')
      vote_item.filter("[vote-direction='up']").addClass('up')
      vote_item.filter("[vote-direction='down']").addClass('down')
    }
  }

  /**
   * Deterine if the vote should be 1, 0 or -1 in direction.
   */
  function determine_vote_direction(vote_status, direction) {
    if(vote_status.match('dislikes') && direction == 'down') { return 0 }
    if(vote_status.match('likes') && !vote_status.match('dislikes') && direction == 'up') { return 0 }
    if(direction == 'up') { return 1 }
    if(direction == 'down') { return -1 }
  }
}
