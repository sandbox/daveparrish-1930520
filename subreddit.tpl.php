<?php

/**
 * @file
 * Default template for the subreddit data.
 */
//echo "<h3>/r/$name.</h3>";
if (!is_array($data) ||
  !array_key_exists('data', $data) ||
  !array_key_exists('children', $data['data'])) :
  echo $data;
else :
  $rank = 0;
  echo '<div><a href="/subreddit/submit">Submit to /r/' . $name . '</a></div>';
  foreach ($data['data']['children'] as $reddit_link) :
    //dpm($reddit_link);
    // Determine the vote status.
    $vote_status_class = 'unvoted';
    $up_vote_class = 'up';
    $down_vote_class = 'down';
    // Base value for determining score.
    $vote_mod_value = 0;
    // Default class for ranking.
    $ranking_class = 'other';
    // This is whether or not the use wants thubmnails on or off.
    // OPTION NOT IMPLMENTED!
    $pics_on = TRUE;
    $rank += 1;
    // Determines classes for certian stories.
    switch ($rank) :
    case 1:
        $ranking_class = 'first';
        break;

    case 2:
        $ranking_class = 'second';
        break;

    case 3:
        $ranking_class = 'third';
        break;
    endswitch;
    if($reddit_link['data']['likes'] === TRUE) :
      $vote_status_class = 'likes';
      $up_vote_class = 'upmod';
      $vote_mod_value = -1;
    endif;
    if($reddit_link['data']['likes'] === FALSE) :
      $vote_status_class = 'dislikes';
      $down_vote_class = 'downmod';
      $vote_mod_value = 1;
    endif;
    echo '<div class="thing ' . $ranking_class . '" data-fullname="' . $reddit_link['data']['name'] . '">';
      echo '<div class="inner-thing ' . $vote_status_class  . '">';
        echo '<div class="rank"><span>' . $rank . '</span></div>';
        echo '<div class="voting">';
          echo '<div class="arrow ' . $up_vote_class . ' vote-action" vote-direction="up">Up</div>';
          echo '<div class="score likes">' .
            ($reddit_link['data']['score'] + 1 + $vote_mod_value) . "</div>";
          echo '<div class="score unvoted">' .
            ($reddit_link['data']['score'] + $vote_mod_value) . "</div>";
          echo '<div class="score dislikes">' .
            ($reddit_link['data']['score'] - 1 + $vote_mod_value) . "</div>";
          echo '<div class="arrow ' . $down_vote_class . ' vote-action" vote-direction="down">Down</div>';
        echo '</div>';
        if ($pics_on) :
            echo '<a class="thumb" href="' .
              _subreddit_url_to_https($reddit_link['data']['url']) . '">';
            $thumbnail_image = array_key_exists('thumbnail', $reddit_link['data']) ?
              $reddit_link['data']['thumbnail'] : 'default';
            if ($thumbnail_image == 'self') :
              echo '<div class="self"></div>';
            elseif ($thumbnail_image == 'default') :
              echo '<div class="default"></div>';
            elseif ($thumbnail_image == 'nsfw') :
              echo '<div class="nsfw"></div>';
            else :
              echo '<img src="' . _subreddit_url_to_https($thumbnail_image) . '"></img>';
            endif;
          echo '</a>';
        elseif(!$pics_on) :
          echo '<a class="default" href="#"></a>';
        else :
          echo '<a class="no-pic"></a>';
        endif;
        echo '<div class="entry">';
        echo l($reddit_link['data']['title'],
          _subreddit_url_to_https($reddit_link['data']['url']),
            array(
              'attributes' => array(
                'class' => 'title',
              ),));
          $domain_link = !strncmp($reddit_link['data']['domain'], 'self.', strlen('self.')) ?
            "" : "https://www.reddit.com/domain/" . $reddit_link['data']['domain'];
          echo '<span class="domain">(<a href="' . $domain_link . '">' . $reddit_link['data']['domain'] . '</a>)</span>';

          $time_ago = format_interval(time() - $reddit_link['data']['created_utc']);
          echo '<p class="tagline">submitted <time title="'
            . date(DATE_RFC850, $reddit_link["data"]["created_utc"]) . '" datetime="'
            . date(DATE_ATOM, $reddit_link["data"]["created_utc"]) . '">'
            . $time_ago . '</time> ago by <a href="https://www.reddit.com/u/'
            . $reddit_link['data']['author'] . '" class="author">' .
            $reddit_link['data']['author'] . '</a><span class="userattrs"></span></p>';
          echo '<ul class="buttons">';
            if ($reddit_link['data']['num_comments'] > 0) :
              echo '<li class="first"><a href="https://www.reddit.com'
                . $reddit_link['data']['permalink'] . '">' . $reddit_link['data']['num_comments'] . ' comments</a></li>';
            else :
              echo '<li class="first"><a href="https://www.reddit.com'
                . $reddit_link['data']['permalink'] . '">comment</a></li>';
            endif;
          echo '</ul>';
        echo '</div>';
      echo '</div>';
    echo '</div>';
  endforeach;
endif;
